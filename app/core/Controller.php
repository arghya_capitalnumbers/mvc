<?php
/*
 * Class Controller
 * Class Controller- The Base controller to extend by the controller files
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 05, 2016
 */
use UserAuth as userIdentification;
use ViewLoader as loadView;
use ModelLoader as loadModel;

class Controller
{
	public $appUserId;
	public $appUserEmail;
	public $string = '';
	public $location;
	
	protected $controller = 'home';
	protected $method = 'index';
	
	public $userAction;
	
	protected $modelName;
	
	public function __construct() {
		$this->_loadSession();
	}
	
	public function model($model)
	{
		return loadModel::model($model);
	}
	
	public function view($view, $data = []) 
	{
		loadView::view($view, $data);
	}
	
	public function loadFullView($view, $data = [])
	{
		loadView::loadFullView($view, $data);
	}
	
	public function userAuthentication($data, $type = NULL)
	{
		return userIdentification::checkAuthentication($data, $type);
	}
	
	public function _loadSession() 
	{
		if ($_SESSION) {
			$this->appUserId = $_SESSION['_id'];
			$this->appUserEmail = $_SESSION['_email'];
			if (isset($_SESSION['_ASSIGNED_ROLES'])) {

				$returnPermission = self::_loadPermissionSet();
				
				if ($returnPermission == false) {
					$this->redirect("home/unauthorized");
				}
				
			}
		}
	}
	
	// ACL mechanism goes here
	public function _loadPermissionSet()
	{
		$permissionArray = [];
		$Roles = new Roles();
		foreach ($_SESSION['_ASSIGNED_ROLES'] as $assignedRoles) {
			$Data = $Roles::select('role_permission.permitted_action')
    				->join('role_permission', 'role_permission.role_id','=', 'role.id')
					->where('role', '=', "$assignedRoles")
					->get();
			foreach ($Data->toArray() as $val) {
				$permissionArray[] = $val;
			}
		}	

		$permissionElements = [];
		foreach ($permissionArray as $val) {
			$permissionElements[] = $val['permitted_action'];		
		}
		
		$url = $this->parseUrl();
		if (file_exists("../app/controllers/" . $url[0] . ".php")) {
			$this->controller = $url[0];
		}
		
		if (isset($url[1])) {
			$this->method = $url[1];
		}
		
		$this->userAction = $this->controller . "/" . $this->method;
		
		$returnAllwaysAllowedActions = self::_loadAlwaysAllowed();
		
		
		if (in_array('ALL', $permissionElements)) {
			return true;											// admin access
		} else if (in_array($this->userAction, $permissionElements)) {
			return true;											// action access permission granted
		} else if ($this->userAction == 'home/unauthorized') {
			return true;											// action access permission granted
		} else if (in_array($this->userAction, $returnAllwaysAllowedActions)) {
			return true;											// allways allowed actions access permission granted
		} else {
			return false;											// action access not permission granted
		}
	}
	
	public function parseUrl()
	{
		if (isset($_GET['url'])) {
			return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
		}
	}
	
	// it takes few actions like aboutus, contactus and make them available for even the guest users.
	public function _loadAlwaysAllowed()
	{
		$allowedAction = [
			'home/index',
			'home/about',
			'home/contact',
			'home/signout',
			'home/signin',
		];
		return $allowedAction;
	}
	
	public function redirect($localtion = '', $string = [])
	{
		if (!empty($string)) {
			$this->string = '?';
			foreach ($string as $key => $value) {
				$this->string.= $key . "=" . $value;
			}
		}
		
		$this->location = APP_URL;
		if (!empty($localtion)) {
			$this->location = APP_URL.$localtion;
		}
		
		header("location: " .$this->location . $this->string);
		exit;
	}
	
}
