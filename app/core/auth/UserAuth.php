<?php
/**
 * class UserAuth
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */

class UserAuth
{
	public $_CODE_;
	
	public function checkAuthentication($data, $type = NULL)
	{
		return self::authenticate($data, $type);
	}

	/**
	 * Authenticates a user.
	 * @return status code whether authentication succeeds.
	 */
	public function authenticate($data, $type = NULL)
	{
		$user = $this->model('Users');
		
		$userDetailsData = $user::where('username', $data['username'])->first();
		if ($userDetailsData) {
			$userData = $userDetailsData->toArray();
			if ($userData['password'] == md5($data['password'])) {
				$this->_CODE_ = '200';
				
				$userRoles = $userDetailsData->userRoles()->get()->toArray();	// calling from model eloquent orm relation
				$roleArray = [];
				foreach ($userRoles as $userRolesValues) {
					$roleArray[] = $userRolesValues['role'];
				}
				self::userSession(
					array(
						'_id' => $userData['id'],
						'_email' => $userData['email'],
						'_ASSIGNED_ROLES' => $roleArray,
					)
				);
			} else {
				$this->_CODE_ = '401';
			}
		} else {
			$this->_CODE_ = '404';
		}
		
		return $this->_CODE_;
		
	}
	
	public function userSession($sessionArray)
	{
		foreach ($sessionArray as $sessionArrayKey => $sessionArrayValue) {
			$_SESSION["$sessionArrayKey"] = $sessionArrayValue;
		}
	}	

}
