<?php
/**
* Configuration file
*
*
* @Package IRA
* @author Arghya Ghosh <arghya@capitalnumbers.com>
* 
*/

// Theme identifiers
define('THEME', 'classic');
define('ADMIN_THEME', 'classic');

// system configuration
define('_PRE_', 'http://');
define('_HOST_', 'localhost/');
define('APP_URL', _PRE_ . _HOST_ . "mvc/public/");
define('_SITE_NAME_', 'iRA');

// Database settings
define('_DB_HOST_', '127.0.0.1');
define('_DB_USERNAME_', 'root');
define('_DB_PASSWORD_', 'root');
define('_DB_NAME_', 'mvc');
define('_DB_TABLE_PREFIX_', 'ira_');
define('_DB_DRIVER_', 'mysql');
define('_DB_CHARSET_', 'utf8');
define('_DB_COLLATION_', 'utf8_unicode_ci');
