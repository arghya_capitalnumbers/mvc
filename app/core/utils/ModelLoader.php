<?php
/*
 * Class ModelLoader
 * Class ModelLoader- Loads model files and object which are subject to return
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 07, 2016
 */
class ModelLoader
{
	public function model($model)
	{
		if (file_exists("../app/models/$model.php")) {
			return new $model();
		} else {
			try {
				throw new ExtendException('404', "- Class $model.php not found!"); 
			} catch (Exception $e) {
				print $e->getMessage();
			}
		}
	}	
}