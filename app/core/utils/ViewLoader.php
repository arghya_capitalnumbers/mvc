<?php
/*
 * Class ViewLoader
 * Class ViewLoader- Loads View files which are subject to return
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 07, 2016
 */
class ViewLoader
{
	public function view($view, $data = [])
	{
		if (file_exists('../app/views/' . $view . '.php')) {
			self::themeLoader($view, $data);
		} else {
			try {
				throw new ExtendException('404', "- View file /app/views/$view.php not found!");
			} catch (Exception $e) {
				print $e->getMessage();
			}
		}
		
	}
	
	public function loadFullView($view, $data = [])
	{
		if (file_exists('../app/views/' . $view . '.php')) {
			self::basicLoader($view, $data);
		} else {
			try {
				throw new ExtendException('404', "- View file /app/views/$view.php not found!");
			} catch (Exception $e) {
				print $e->getMessage();
			}
		}
		
	}
	
	private function themeLoader($view, $data = [])
	{
		require_once '../public/theme/' . ADMIN_THEME . '/_header.php';
		require_once '../app/views/' . $view . '.php';
		require_once '../public/theme/' . ADMIN_THEME . '/_footer.php';
	}
	
	private function basicLoader($view, $data = [])
	{
		require_once '../app/views/' . $view . '.php';
	}
	
}