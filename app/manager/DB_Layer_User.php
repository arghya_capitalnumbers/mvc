<?php 
/*
 * class DB_Layer_User
 * This is for the extra layer of data abstraction
 * 
 * WEB bits
 * detailsData()-  returns specific post related data
 * listData()-  returns all users
 * 
 * API bits
 * detailsDataApi()-  returns specific post related data for api purpose
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 05, 2016
 */
class DB_Layer_User extends Controller 
{
	protected $modelName = 'Users';
	
	public function __construct() {
		parent::__construct();
	}
	
	/*
	 * method detailsData()
	 * param $id integer
	 *  
	 * returns array()
	 */
	public function detailsData($id = '') 
	{
		$user = $this->model($this->modelName);
		if ((int)$id) {
			$userDetailsData = $user::where('id', $id)->first();
			if ($userDetailsData) {
				$this->view('user/_details', ['data' => $userDetailsData->toArray()]);
			} else {
				echo "No record found";
			}
		} else {
			echo "Bad request";
		}
	}
	
	/*
	 * method listData()
	 *  
	 * returns array()
	 */
	public function listData()
	{
		$user = $this->model($this->modelName);
		$userDetailsData = $user::select('id', 'email', 'username', 'firstname', 'lastname')
					->get();
		if ($userDetailsData) {
			$this->view('user/_list', ['data' => $userDetailsData->toArray()]);
		} else {
			echo "No record found";
		}
	}
	
	/*
	 * method detailsDataApi()
	 * param $id integer
	 *  
	 * returns array() for api response
	 */
	public function detailsDataApi($id = '') 
	{
		$user = $this->model($this->modelName);
		if ((int)$id) {
			$userDetailsData = $user::where('id', $id)->first();
// 			$userDetailsData = $user::select('id', 'email', 'username', 'firstname', 'lastname')
// 						->where('id', '=', "$id")
// 						->get();
			if (count($userDetailsData)) {
				return array(
					'response_code' => '200',
					'response_mesg' => 'Record fetched successfully',
					'data' 			=> $userDetailsData
				);
			} else {
				return array(
					'response_code' => '404',
					'response_mesg' => 'Record not found',
				);
			}
		} else {
			return array(
				'response_code' => '405',
				'response_mesg' => 'Required parameter(s) not provided',
			);
		}
	}
	
}