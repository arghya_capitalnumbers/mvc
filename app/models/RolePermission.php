<?php
/*
 * Class RolePermission
 * Model class file
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 13, 2016
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class RolePermission extends Eloquent
{
	protected $id;
	protected $role_id; 
	protected $permitted_action; 
	
	public $table = "role_permission";
	protected $fillable = [];

	/**
	 * constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * reset
	 *
	 * Reset values of all member variables.
	 */
	public function reset() {
		$this->id = null;
		$this->role_id = null;
		$this->permitted_action = null;
	}
}
