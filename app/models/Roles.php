<?php
/*
 * Class Roles
 * Model class file
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 12, 2016
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class Roles extends Eloquent
{
	protected $id;
	protected $role; 
	
	protected $name;
	
	public $table = "role";
	protected $fillable = ['role'];

	/**
	 * constructor
	 */
	public function __construct() 
	{
		parent::__construct();
	}

	/**
	 * reset
	 *
	 * Reset values of all member variables.
	 */
	public function reset() 
	{
		$this->id = null;
		$this->role = null;
	}
	
}
