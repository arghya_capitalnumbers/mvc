<?php
/*
 * Class UserRoles
 * Model class file
 * 
 * @author Arghya Ghosh <arghya@capitalnumbers.com>
 * @date Sept 12, 2016
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class UserRoles extends Eloquent
{
	protected $id;
	protected $user_id;
	protected $role_id; 
	
	public $table = "user_role";
	protected $fillable = [];

	/**
	 * constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * reset
	 *
	 * Reset values of all member variables.
	 */
	public function reset() {
		$this->id = null;
		$this->user_id = null;
		$this->role_id = null;
	}
	
}
