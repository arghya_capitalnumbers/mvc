$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$("#forrecpass").fadeOut(100);
		$(this).addClass('active');
		e.preventDefault();
	});
	
	$('#fp').click(function(e) {
		$("#register-form").fadeOut(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$('#register-form-link').removeClass('active');
		$("#forrecpass").delay(100).fadeIn(100);
		e.preventDefault();
	});

    $('#bklog').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
		$('#login-form-link').addClass('active');
		$("#forrecpass").fadeOut(100);
		e.preventDefault();
	});

});
